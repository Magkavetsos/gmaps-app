import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  protected lat: number = 37.945369;
  protected lng: number = 23.672789;

  protected lat1: number = 37.945837;
  protected lat2: number = 37.934951;
  protected lat3: number = 37.949761;


  protected lng1: number = 23.649493;
  protected lng2: number = 23.693461;
  protected lng3: number = 23.643011;

  constructor(public navCtrl: NavController) {

  }

  protected changeIcon(e,marker) {
  }

}

